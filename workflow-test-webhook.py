#!/usr/bin/python3

from flask import Flask, request, abort
import requests
import json
import requests
import os
import threading
from tasks import evaluate_mr

app = Flask(__name__)

shutdown = False
tasklist = None
notify = None
tasklock = None
mrlists = None

def thread_log(prefix, msg):
    print("[" + prefix + str(threading.current_thread().ident) + "] " + msg + "\n", flush=True)

def completor_log(msg):
    thread_log("Completor", msg)

def dispatcher_log(msg):
    thread_log("Dispatcher", msg)

def work_completor(mriid, mrlock):
    global shutdown
    global mrlists
   
    while shutdown == False: 
        mrlock.acquire()
        completor_log("Getting new job for mriid " + mriid)
        try:
            mrlist = mrlists[mriid]
            newtask = mrlist[0]
            mrlist = mrlist[1:]
            # Need to write the list back so the dispatcher sees it
            mrlists[mriid] = mrlist
        except:
            # no new tasks, we can exit
            completor_log("queue empty for mriit " + mriid)
            # Check to see if the list is empty, if it is, we can remove it and
            # exit
            if len(mrlist) == 0:
                completor_log("removing queue for mriid " + mriid)
                del mrlists[mriid]
            mrlock.release()
            break

        mrlock.release()
        completor_log("issuing rabbit tasks")
        results = evaluate_mr.delay(newtask['json'])
        completor_log("checking rabbit task results")
        try:
            for r in results.collect():
                completor_log("Got a task result, waiting for more")
        except Exception as e:
            completor_log("Hit an exception in results: " + str(e)) 
        completor_log("rabbit task results all in, checking for more work")

    # If we get here, we're done and can exit
    completor_log("exiting")
    return


def get_mriid(payload):
    if payload['object_kind'] == 'pipeline':
        if payload['merge_request'] == None:
            return None
        return str(payload['merge_request']['iid'])
    elif payload['object_kind'] == 'note':
        return str(payload['merge_request']['iid'])
    elif payload['object_kind'] == 'merge_request':
        return str(payload['object_attributes']['iid'])
    else:
        dispatcher_log("Doing nothing with object kind " + payload['object_kind'])
    return None

def work_dispatcher(notify, tasklock):
    global shutdown
    global tasklist
    global mrlists

    dispatcher_log("Starting dispatcher")
    mrlists = {}
    mrlock = threading.Lock()
    mrtasks = [] 

    while shutdown == False:
        dispatcher_log("waiting for work")
        notify.acquire()
        dispatcher_log("got new dispatch request")
        tasklock.acquire()
        try:
            newtask = tasklist[0]
            tasklist = tasklist[1:]
        except:
            dispatcher_log("dispatch queue empty!  We may be shutting down")
            tasklock.release()
            continue
        tasklock.release()
        mriid = get_mriid(newtask['json'])
        if mriid == None:
            continue
        mrlock.acquire()
        if mriid in mrlists:
            dispatcher_log("appending job for mriid " + mriid)
            mrlists[mriid].append({'json': newtask['json']})
        else:
            dispatcher_log("creating new queue for mriid " + mriid)
            mrlists[mriid] = []
            mrlists[mriid].append({'json': newtask['json']})
            dispatcher_log("starting completor task for queue on mriid " + mriid)
            newtsk = threading.Thread(target=work_completor,
                        args=(mriid, mrlock))
            newtsk.start()
            # Make sure the new task activates
            while newtsk.is_alive() == False:
                time.sleep(0)
            mrtasks.append(newtsk)
        mrlock.release()
        # Clean up the task list
        dispatcher_log("cleaning completor tasks")
        killtasks = []
        for t in mrtasks:
            if t.is_alive() == True:
                dispatcher_log("completor " + str(t.ident) + " is still working")
                continue
            killtasks.append(t)
        for t in killtasks:
            dispatcher_log("completor " + str(t.ident) + " is done, reaping")
            mrtasks.remove(t)
            t.join()


    dispatch_log("shutdown....terminate completors")
    # If we get here, we're shutting down, join the outstanding threads
    # and leave
    mrlock.acquire()
    # make sure none of the completion tasks have work to do
    mrlists = {}
    mrlock.release()
    for t in mrtasks:
        dispatch_log("joining completor " + str(t.ident))
        t.join()
    return
            
@app.route("/", methods=['GET', 'POST'])
def rcvhook():
    global tasklist
    global notify
    global tasklock

    print("Getting incomming message\n") 
    try: 
        payload = json.loads(request.data)    
    except:
        print("Invalid Json, skipping\n")
        return 'OK'
    newtask = {'json': payload}
    print(newtask)
    tasklock.acquire()
    tasklist.append(newtask)
    tasklock.release()
    notify.release()
    
    return 'OK'

def main():
    global shutdown
    global tasklock
    global notify
    global tasklist

    notify = threading.Semaphore(value=0)
    tasklock = threading.Lock()
    tasklist = []
    dispatchthread = threading.Thread(target=work_dispatcher, args=(notify, tasklock))
    dispatchthread.start()
    
    try:    
        port_number = os.environ['WEBHOOK_PORT']
    except:
        print("No port specified, defaulting to 5001\n")
        port_number = 5001

    app.run(host="0.0.0.0", port=port_number)

    print("Shutting Down\n")
    shutdown = True
    notify.release()
    dispatchthread.join()
    
if __name__ == "__main__":
    main()
