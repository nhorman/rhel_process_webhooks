#!/usr/bin/python3

import requests
import json
import gitlab
import requests
import git
import tempfile
import os
from enum import IntEnum
from celery import Celery
from celery.signals import worker_init

gitdir = None

app = Celery('tasks', backend='rpc://', broker=os.environ['AMQP_BROKER'])


class NoPipelineException(Exception):
    def __init__(self, message):
        self.message = message

class NotApprovedException(Exception):
    def __init__(self, message):
        self.message = message

def extract_bzs(commit):
    mlines = commit.message.split('\n')
    for l in mlines:
        if l.startswith('BZ:'):
            l = l[3:]
            l = l.replace(',', ' ')
            return l.split()
    raise Exception('No BZs Found in commit ' + str(commit))

def extract_files(commit):
    filelist = []
    diffstats = commit.stats.files
    diff = commit.diff()
    print(diff)
    for d in diff:
        print(d)
        filelist.append(d.new_path)
    print(filelist)
    return filelist

def validate_bzs(reviewed_items, user, mr, project):
    common_tgt_release = None
    for i in reviewed_items:
        bzs_valid = []
        i['commit_valid'] = False
        if not i['bzs']:
            i['bzs'] = []
            continue
        i['commit_valid'] = True
        for j in i['bzs']:
            print("Checking on bz %s\n" % str(j))
            bug = requests.get('https://bugzilla.redhat.com/rest/bug?id='+str(j)+'&api_key='+os.environ['BZ_API_KEY'])
            bug = json.loads(bug.text)
            bug = bug['bugs'][0]
            if 'cf_internal_target_release' in bug:
                print("BZ target release is %s\n" % bug['cf_internal_target_release'])
                if common_tgt_release == None:
                    common_tgt_release = bug['cf_internal_target_release']
                bz_properties = {'bz':j, 'notes': []}
                buginputs = {
                    'package': 'kernel',
                    'namespace': 'rpms',
                    'ref': 'refs/heads/rhel-'+bug['cf_internal_target_release'],
                    'commits': [{
                        'hexsha': str(i['commit']),
                        'files': i['files'],
                        'resolved': [int(j)],
                        'related': [],
                        'reverted': [],
                    }]
                }
                print(json.dumps(buginputs, indent=2))
                # NEED TO FIND A BETTER WAY TO DO THIS - FIXME!
                bugresults = requests.post("https://10.19.208.80/lookaside/gitbz-query.cgi", json=buginputs, verify=False)

                resjson = bugresults.json()
                print(json.dumps(resjson, indent=2))
                if bug['cf_internal_target_release'] != common_tgt_release:
                    print("Multiple target releases found!\n")
                    bz_properties['approved'] = False
                    bz_properties['notes'].append("bz target release mismatch\n")
                else:
                    if resjson['result'] == 'ok':
                        bz_properties['approved'] = True
                    else:
                        bz_properties['approved'] = False
                        errorstring = resjson['error'].replace('\n', ' ')
                        bz_properties['notes'].append(errorstring)
                    bz_properties['logs'] = resjson['logs']
            else:
                bz_properties = {'bz':j, 'notes': []}
                bz_properties['approved'] = False
                bz_properties['notes'].append("Bug has no internal target release defined")
            bzs_valid.append(bz_properties)
        i['bzs'] = bzs_valid

def print_bz_report(project, mr, reviewed_items):
    mr_approved = True
    report = ""
    table = []
    for i in reviewed_items:
        if i['commit_valid'] == False:
            print("Commit is not valid")
            mr_approved = False
        if mr_approved == True:
            table.append([i['commit'], "", "", ""])
        else:
            table.append([i['commit'], "", "", "Commit contains no valid bzs, please add a line to the changelog\nin the format BZ: <bugzilla>[,<bugzilla>...]"])
        for j in i['bzs']:
            if j['approved'] == False:
                mr_approved = False
            notestring = ""
            for n in j['notes']:
                notestring = notestring + n + "<br>"
            table.append(["", j['bz'], j['approved'], notestring])
    report = "Project: " + project.name_with_namespace + "   \n"
    report += "Project ID: " + str(project.id) + "   \n"
    report += "Merge Request ID: " + str(mr.iid) + "   \n"
    report += "Target Branch: " + mr.target_branch + "   \n"
    report += "Note that acceptance rules are based on target branch and   \n"
    report += "are established by Red Hat policy.  If a given bz fails   \n"
    report += "validation, please submit a query in that bugzilla   \n"
    report += " \n"
    report += "BZ READINESS REPORT:\n\n"
    report += "|Commit|BZ|Approved|Notes|\n"
    report += "|:------|:------|:------|:-------|\n"
    for r in table:
        report += "|"+str(r[0])+"|"+r[1]+"|"+str(r[2])+"|"+r[3]+"|\n"
    
    report += "\n\n"
    if mr_approved == True:
        report += "Merge Request passes bz validation\n"
    else:
        report += "\nMerge Request fails bz validation.  \n"
        report += " \n"
        report += "To request re-evalution after getting bz approval "
        report += "add a comment to this MR with only the text: "
        report += "request-bz-evaluation "

    return (report, mr_approved)

def check_on_bzs(lab, mr, project):
    global repo
    review_lists = []
    for c in mr.commits():
        commit = project.commits.get(c.id) 
        try:
            found_bzs = extract_bzs(commit)    
        except:
            found_bzs = []
        try:
            found_files = extract_files(commit)
        except:
            found_files = []

        review_lists.append({'commit': commit.id, 'bzs': found_bzs, 'files': found_files})
    try:
        user = lab.users.get(mr.author['id'])
    except:
        user = None
    validate_bzs(review_lists, user, mr, project)
    return print_bz_report(project, mr, review_lists)

def add_label(mr, label):
    if label not in mr.labels:
        print("Adding label %s to MR %d\n" % (label, mr.iid))
        mr.labels.append(label)
        return True
    print("Skipping addition of label %s to MR %d, already there\n" % (label, mr.iid))
    return False

def remove_label(mr, label):
    if label in mr.labels:
        print("Removing label %s from MR %d\n" % (label, mr.iid))
        mr.labels.remove(label)
        return True
    print("Skipping label %s removal from MR %d, not there\n" % (label, mr.iid))
    return False    

def check_ci_result(lab, mr, project, payload):
        message = "Still waiting for CI to complete"
        print("Checking CI state\n", flush=True)
        # We're running because we got an MR update
        if mr.pipeline != None:
            if mr.pipeline['status'] == 'success':
                return True
            else:
                return False
        else:
            print("MR %d Has no CI pipeline, skipping check\n" % mr.iid)
            
        return False

def check_approvals(lab, mr, project):
        approvals = mr.approvals.get()
        print("We have " + str(len(approvals.approved_by)) + " approvals")
        if len(approvals.approved_by) >= 1:
            return True
        return False

def run_bz_validation(lab, mr, project):
        print("Running bz validation for MR %d\n" % mr.iid)
        (report, approved) = check_on_bzs(lab, mr, project)
        #mr.notes.create({'body': report})
        if (approved == True):
            print("WE PASSED BZ VALIDATION\n")
            mr.labels.append('bzValidated')
        else:
            print("WE FAILED BZ VALIDATION!\n")
            # This is not really part of the state machine, but rather a marker,
            # to prevent us from having to create several more states to
            # consider
            mr.labels.append('bzValidationFails')
        return report

def check_bz_valid(lab, mr, project):
    if 'bzValidated' in mr.labels:
        return True
    return False

def check_ready_for_merge(lab, mr, project, payload):
    print("Checking ready for merge status on MR %d\n" % mr.iid)
    if check_bz_valid(lab, mr, project) == False:
        print("Bugzilla state invalid for MR %d\n" % mr.iid)
        return False
    if check_ci_result(lab, mr, project, payload) == False:
        print("CI Status invalid for MR %d\n" % mr.iid)
        return False
    if check_approvals(lab, mr, project) == False:
        print("Insufficient approvals\n")
        return False
    for l in mr.labels:
        if l.startswith('NACK'):
            print("Not ready for merge due to NACKS\n")
            return False
    print("MR %d is ready for merge\n" % mr.iid)
    return True

def get_project_and_mr(lab, payload):
    project = None
    mr = None
    print("Getting Project id %d\n" % payload['project']['id'])
    project = lab.projects.get(payload['project']['id'])
    try:
        if payload['object_kind'] == 'note':
            mr = project.mergerequests.get(payload['merge_request']['iid'])
        else:
            mr = project.mergerequests.get(payload['object_attributes']['iid'])
    except gitlab.exceptions.GitlabHttpError as e:
        print("Could not find MR in project %d\n" % project.id)
        raise Exception("Unable to find MR")
    return (project, mr, mr.labels.copy().sort())

def save_mr_if_changed(mr, oldlabels):
    newlabels = mr.labels.copy().sort()
    if oldlabels == newlabels:
        print("Saving MR back to server\n")
        mr.save()

def apply_or_remove_label(label, mr, applyorremove):
    if applyorremove == True:
        add_label(mr, label)
    else:
        remove_label(mr, label)
    return applyorremove

@app.task
def evaluate_mr(payload):
    global gitdir
    lab = gitlab.Gitlab(os.environ['GITLAB_HOST'], private_token=os.environ['GITLAB_API_KEY'], ssl_verify=False)
    mr = None
    project = None
    print(json.dumps(payload, indent=2))
    print("Handling %s request\n" % payload['object_kind'])
    if payload['object_kind'] == 'note':
        print("Checking note request\m")
        notetext = payload['object_attributes']['note']
        (project, mr, oldlabels) = get_project_and_mr(lab, payload)

        # Filter out MRs that are not in the opened state
        if mr.state != 'opened':
            print("MR %d is in state %s, ignoring\n" % (mr.iid, mr.state)) 
            return

        if notetext.startswith("request-bz-evaluation"):
            # Force a re-run of the bz validation
            remove_label(mr, 'bzValidationFails')
            remove_label(mr, 'bzValidated')
            report = run_bz_validation(lab, mr, project)
            save_mr_if_changed(mr, oldlabels)
            mr.notes.create({'body': report})
            return
        elif notetext.startswith("MR-NACK:"):
            print("Detectd MR NACK\n")
            nackemail = payload['user']['email']
            # don't allow NACKs from non-redhatters
            print("Checking email %s\n" % nackemail)
            if not nackemail.endswith("@redhat.com"):
                return
            nackuser = payload['user']['username']
            nacklabel="NACK:"+nackuser
            try:
                print("Removing old labels\n")
                mr.labels.remove(nacklabel)
            except:
                print("Got exception\n")
                pass
            print("Adding new nacklabel\n")
            try:
                print("Remove readyForMerge on NACK")
                mr.labels.remove("readyForMerge")
            except:
                pass
            mr.labels.append(nacklabel)
            save_mr_if_changed(mr, oldlabels)
            return
        elif notetext.startswith("MR-NACK-RESCIND:"):
            needupdate = False
            print("Detectd MR NACK RESCIND\n")
            nackemail = payload['user']['email']
            # don't allow NACKs from non-redhatters
            print("Checking email %s\n" % nackemail)
            if not nackemail.endswith("@redhat.com"):
                return
            nackuser = payload['user']['username']
            nacklabel="NACK:"+nackuser
            try:
                print("Removing NACK labels for %s\n" % nacklabel)
                mr.labels.remove(nacklabel)
            except:
                print("No such NACK label\n")
                pass
            if check_ready_for_merge(lab, mr, project, payload) == True:
                if 'readyForMerge' not in mr.labels:
                    mr.labels.append('readyForMerge')
            save_mr_if_changed(mr, oldlabels)
        else:
            # on any other note update, we want to check our CI status and maybe 
            # set the ready for merge flag
            apply_or_remove_label('readyForMerge', mr, 
                                 check_ready_for_merge(lab, mr, project, payload))
            save_mr_if_changed(mr, oldlabels)
            return

    elif payload['object_kind'] == 'merge_request':
        report = None
        (project, mr, oldlabels) = get_project_and_mr(lab, payload)
        if mr.state == 'closed':
            print("MR %d is closed, skipping\n" % mr.iid)
            return
        # only do bz validation if we haven't done it already
        if 'oldrev' in payload['object_attributes']:
            print("This is a push event, re-running bz validation\n")
            remove_label(mr, 'bzValidationFails')
            remove_label(mr, 'bzValidated')

        if 'bzValidated' not in mr.labels and 'bzValidationFails' not in mr.labels:
            report = run_bz_validation(lab, mr, project)
        apply_or_remove_label('readyForMerge', mr,
                             check_ready_for_merge(lab, mr, project, payload))
        save_mr_if_changed(mr, oldlabels)
        if report:
            mr.notes.create({'body': report})

    else:
        print("Doing nothing with object kind " + payload['object_kind'])
        return

