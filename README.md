rhel_process_webhook

1. Overview
This project is meant to provide infrastucture in gitlab to implement review process requirements for RHEL development  Specifially:
    - It will preform bugzilla validation on any opened merge requests for a project using these hooks using RHELs internal dist git service.  Failure or success to validate a given MR will result in a bzValidated or bzFailsValidation label being applied to the MR.  Note all commits in an MR must specify the bzs they address by adding a line to the changelog for the commit prefixed with the string "BZ:" followed by a comma separated list of bzs
    - It will confirm sufficient merge request approval are asserted on a merge request, that CI has passed, and that there are no outstanding labels on the MR prefixed with the string NACK:.  Passing these tests will result in the application of a readyForMerge label on the MR, serving as an indicator that the MR is ready to be accepted in the target branch

3. Requirements
- This project must have a public facing ip address configured on the OCP cluster being used
- This project must also have access to the internal RHEL dist-git instance (to process bz validation)

2. Installation
This project is intended to be run in a set of containers on an OCP instance.  To install:
    - Enter the configs directory
    - Edit the makefile there with the appropriate config settings
    - Run make
    - run oc create -f project.yaml to deploy the project



